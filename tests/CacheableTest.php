<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls\Tests;

class CacheableTest extends RussianDollsTestCase
{
    public function testGetCacheKey(): void
    {
        $post = $this->makePost();

        self::assertEquals(
            "SlyFoxCreative\\RussianDolls\\Tests\\Post/1-{$post->updated_at->timestamp}",
            $post->getCacheKey(),
        );
    }
}
