<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls\Tests;

use Illuminate\Database\Capsule\Manager as DB;
use PHPUnit\Framework\TestCase;

class RussianDollsTestCase extends TestCase
{
    protected function setUp(): void
    {
        $this->setUpDatabase();
        $this->migrateTables();
    }

    protected function makePost(): Post
    {
        $post = new Post();
        $post->title = 'Test Title';
        $post->save();

        return $post;
    }

    protected function makeComment(): Comment
    {
        $comment = new Comment();
        $comment->title = 'Test Title';
        $comment->save();

        return $comment;
    }

    private function setUpDatabase(): void
    {
        $db = new DB();
        $db->addConnection(['driver' => 'sqlite', 'database' => ':memory:']);
        $db->bootEloquent();
        $db->setAsGlobal();
    }

    private function migrateTables(): void
    {
        DB::schema()->create('posts', function ($table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });
        DB::schema()->create('comments', function ($table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });
    }
}
