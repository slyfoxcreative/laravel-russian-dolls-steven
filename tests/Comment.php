<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls\Tests;

use Illuminate\Support\Carbon;

/**
 * @property Carbon $updated_at
 * @property string $title
 */
class Comment extends \Illuminate\Database\Eloquent\Model
{
    use \SlyFoxCreative\RussianDolls\Cacheable;
}
