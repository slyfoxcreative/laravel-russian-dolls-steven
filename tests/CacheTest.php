<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls\Tests;

use Illuminate\Cache\ArrayStore;
use Illuminate\Cache\Repository;
use Illuminate\Contracts\Cache\Repository as RepositoryContract;
use SlyFoxCreative\RussianDolls\Cache;

class CacheTest extends RussianDollsTestCase
{
    public function testCaching(): void
    {
        app()->singleton(RepositoryContract::class, function () {
            return new Repository(
                new ArrayStore(),
            );
        });
        $cache = new Cache();
        $post = $this->makePost();

        $cache->put($post->getCacheKey(), '<div>fragment</div>');

        self::assertTrue($cache->has($post->getCacheKey()));
    }
}
