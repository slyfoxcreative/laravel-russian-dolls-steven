<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls\Tests;

use Illuminate\Cache\ArrayStore;
use Illuminate\Cache\Repository;
use Illuminate\Contracts\Cache\Repository as RepositoryContract;
use SlyFoxCreative\RussianDolls\BladeDirective;
use SlyFoxCreative\RussianDolls\Cache;
use SlyFoxCreative\RussianDolls\CacheKeyException;

class BladeDirectiveTest extends RussianDollsTestCase
{
    public function tearDown(): void
    {
        \Mockery::close();
    }

    public function testSetUpAndTearDown(): void
    {
        app()->singleton(RepositoryContract::class, function () {
            return new Repository(
                new ArrayStore(),
            );
        });
        $cache = new Cache();
        $directive = new BladeDirective($cache);
        $post = $this->makePost();

        self::assertFalse($directive->setUp($post));

        echo '<div>fragment</div>';

        self::assertEquals('<div>fragment</div>', $directive->tearDown());
        self::assertTrue($cache->has($post->getCacheKey()));
    }

    public function testUncacheableObject(): void
    {
        app()->singleton(RepositoryContract::class, function () {
            return new Repository(
                new ArrayStore(),
            );
        });
        $cache = new Cache();
        $directive = new BladeDirective($cache);
        $object = new \stdClass();

        $this->expectException(CacheKeyException::class);

        $directive->setUp($object);
    }

    public function testStringCacheKey(): void
    {
        $cache = \Mockery::mock(Cache::class);
        $directive = new BladeDirective($cache);

        $cache->expects()->has('testkey');
        $cache->expects()->put('testkey', 'test')->andReturn('test');

        $directive->setUp('testkey');

        echo 'test';

        self::assertSame('test', $directive->tearDown());
    }

    public function testCollectCacheKey(): void
    {
        $cache = \Mockery::mock(Cache::class);
        $directive = new BladeDirective($cache);
        $collection = collect(['one', 'two']);

        $key = md5((string) $collection);

        $cache->expects()->has($key);
        $cache->expects()->put($key, 'test')->andReturn('test');

        $directive->setUp($collection);

        echo 'test';

        self::assertSame('test', $directive->tearDown());
    }

    public function testModelCacheKey(): void
    {
        $cache = \Mockery::mock(Cache::class);
        $directive = new BladeDirective($cache);
        $post = $this->makePost();

        $key = "SlyFoxCreative\\RussianDolls\\Tests\\Post/1-{$post->updated_at->timestamp}";

        $cache->expects()->has($key);
        $cache->expects()->put($key, 'test')->andReturn('test');

        $directive->setUp($post);

        echo 'test';

        self::assertSame('test', $directive->tearDown());
    }

    public function testCacheKeyWithSuffix(): void
    {
        $cache = \Mockery::mock(Cache::class);
        $directive = new BladeDirective($cache);
        $post = $this->makePost();

        $key = "SlyFoxCreative\\RussianDolls\\Tests\\Post/1-{$post->updated_at->timestamp}/testkey";

        $cache->expects()->has($key);
        $cache->expects()->put($key, 'test')->andReturn('test');

        $directive->setUp($post, 'testkey');

        echo 'test';

        self::assertSame('test', $directive->tearDown());
    }
}
