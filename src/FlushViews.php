<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FlushViews
{
    public function handle(Request $request, \Closure $next): Response
    {
        \Cache::tags('views')->flush();

        return $next($request);
    }
}
