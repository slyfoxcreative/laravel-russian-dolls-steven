<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls;

use Illuminate\Contracts\Cache\Repository;

class Cache
{
    public function __construct() {}

    public function put(string $key, string $fragment): string
    {
        return app(Repository::class)
            ->tags('views')
            ->rememberForever($key, function () use ($fragment) {
                return $fragment;
            })
        ;
    }

    public function has(string $key): bool
    {
        return app(Repository::class)->tags('views')->has($key);
    }
}
