<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls;

use Illuminate\Support\Collection;

class BladeDirective
{
    /** @var Collection<int, string> */
    private Collection $keys;

    public function __construct(private Cache $cache)
    {
        $this->keys = new Collection();
    }

    public function setUp(mixed $value, ?string $suffix = null): bool
    {
        $key = $this->normalizeKey($value, $suffix);

        ob_start();

        $this->keys->push($key);

        return $this->cache->has($key);
    }

    public function tearDown(): string
    {
        $key = $this->keys->pop();

        if (! is_string($key)) {
            throw new TearDownException('Tried to pop a nonexistent cache key');
        }

        $buffer = ob_get_clean();

        if ($buffer === false) {
            throw new TearDownException('Output buffering is not active');
        }

        return $this->cache->put($key, $buffer);
    }

    private function normalizeKey(mixed $value, ?string $suffix = null): string
    {
        $normalizedKey = null;

        if (is_string($value)) {
            $normalizedKey = $value;
        }

        if (is_object($value) && method_exists($value, 'getCacheKey')) {
            $normalizedKey = $value->getCacheKey();
        }

        if ($value instanceof Collection) {
            $normalizedKey = md5((string) $value);
        }

        if (is_null($normalizedKey)) {
            throw new CacheKeyException('Could not determine an appropriate cache key.');
        }

        return is_null($suffix) ? $normalizedKey : "{$normalizedKey}/{$suffix}";
    }
}
