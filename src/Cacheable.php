<?php

declare(strict_types=1);

namespace SlyFoxCreative\RussianDolls;

use function SlyFoxCreative\Utilities\assert_instance_of;

trait Cacheable
{
    public function getCacheKey(): string
    {
        $key = $this->getKey();
        $this->assertKeyType($key);

        return sprintf(
            '%s/%s-%s',
            get_class($this),
            $key,
            $this->updated_at->timestamp,
        );
    }

    /**
     * @phpstan-assert int|string|\Stringable $key
     */
    private function assertKeyType(mixed $key): void
    {
        if (is_int($key) || is_string($key)) {
            return;
        }

        assert_instance_of($key, \Stringable::class);
    }
}
